#
# Executes commands at the start of an interactive session.
#
# Authors:
#   Sorin Ionescu <sorin.ionescu@gmail.com>
#

# Source Prezto.
if [[ -s "${ZDOTDIR:-$HOME}/.zprezto/init.zsh" ]]; then
  source "${ZDOTDIR:-$HOME}/.zprezto/init.zsh"
fi
export EDITOR=vim
export VISUAL=vim
# Customize to your needs...
source $HOME/.rvm/scripts/rvm
unsetopt auto_name_dirs
export LC_ALL=en_US.UTF-8
export LANG=en_US.UTF-8